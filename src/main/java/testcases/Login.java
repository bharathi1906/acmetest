package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Login {

	@Test
	public void loginAcme() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.findElementById("email").sendKeys("jayabharathi.sambamoorthi@gmail.com");
		driver.findElementById("password").sendKeys("Ralph@1906");
		
		driver.findElementById("buttonLogin").click();
		Thread.sleep(1000);
	
		Actions action = new Actions(driver);
		WebElement eleVendors = driver.findElementByXPath("//div[@class='dropdown']/button[text()=' Vendors']");
		action.moveToElement(eleVendors).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		
		driver.findElementById("vendorTaxID").sendKeys("RO094782");
		driver.findElementById("buttonSearch").click();
		
		System.out.println(driver.findElementByXPath("//table[@class='table']/tbody/tr/following::tr/td").getText());
		
		driver.close();
	}
}
